import './index.css';
import SidePanel from './sections/SidePanel';
import { useEffect } from 'react';
import BasicData from './sections/BasicData';
import Skills from './sections/Skills';
import Experience from './sections/Experience';
import SideProjects from './sections/SideProjects';
import Education from './sections/Education';

export function App() {
    useEffect(() => {
        if (
            localStorage.getItem('dark-theme') !== 'false' &&
            window.matchMedia &&
            window.matchMedia('(prefers-color-scheme: dark)').matches
        ) {
            document.body.classList.add('dark-theme');
        }

        window.matchMedia &&
            window.matchMedia('(prefers-color-scheme: dark)').addListener(function (e) {
                if (e.matches) {
                    document.body.classList.add('dark-theme');
                } else {
                    document.body.classList.remove('dark-theme');
                }
            });
    }, []);

    return (
        <>
            <SidePanel />
            <BasicData />
            <Skills />
            <Experience />
            <SideProjects />
            <Education />
        </>
    );
}
