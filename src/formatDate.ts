const formatDate = (date: string | null) =>
    date
        ? new Date(date).toLocaleDateString('en-GB', {
              year: 'numeric',
              month: 'short',
          })
        : 'now';

export default formatDate;
