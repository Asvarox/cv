import React from 'react';
import styles from './BasicData.module.css';
import basicData from '../data/basicData.json';
import contactInfo from '../data/contactInfo.json';
export default function BasicData() {
    return (
        <>
            <h1 className={styles.title}>{basicData.name}</h1>
            <p className={styles.position}>{basicData.position}</p>
            <p className={styles.description}>Passionate Frontend Tech Leader and Developer, focused on enabling peers to deliver the highest possible quality business value within time constraints through development environment improvements, processes implementation, and mentorship.</p>
            <div className={styles.webaddress}>
                {contactInfo.email && (
                    <a href={`mailto:${contactInfo.email}`}>
                        <i className="fas fa-at"></i> {contactInfo.email}
                    </a>
                )}
                {(contactInfo.email || contactInfo.phoneNumber) && ' | '}
                {contactInfo.phoneNumber && (
                    <a href={`tel:${contactInfo.phoneNumber}`}>
                        <i className="fas fa-phone"></i> {contactInfo.phoneNumber}
                    </a>
                )}
                {(contactInfo.email || contactInfo.phoneNumber) && <br/>}
                {basicData.links.map((link, index) => (
                    <React.Fragment key={link.name}>
                        <a href={link.url}>
                            <i className={`fab ${link.icon}`}></i> {link.name}
                        </a>
                        {index < basicData.links.length - 1 && ' | '}
                    </React.Fragment>
                ))}
            </div>
        </>
    );
}
