import React from 'react';
import education from '../data/education.json';
import Section from './components/Section';
import T from './components/Typgraphy';
import formatDate from '../formatDate';

export default function Education() {
    return (
        <Section.Container>
            <Section.Header>Education</Section.Header>
            {education.map((row) => (
                <React.Fragment key={row.name}>
                    <Section.Row>
                        <Section.RowLeft>
                            <T.CompanyName>{row.name}</T.CompanyName>
                            <T.Side>
                                ({formatDate(row.start)}-{formatDate(row.end)})
                            </T.Side>
                        </Section.RowLeft>
                        <Section.Nested>
                            <Section.Row>
                                <Section.RowContent>{row.course && <T.P>{row.course}</T.P>}</Section.RowContent>
                            </Section.Row>
                        </Section.Nested>
                    </Section.Row>
                </React.Fragment>
            ))}
        </Section.Container>
    );
}
