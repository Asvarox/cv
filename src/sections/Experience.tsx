import React from 'react';
import experience from '../data/experience.json';
import Section from './components/Section';
import T from './components/Typgraphy';
import Technologies from './Experience/Technologies';
import formatDate from '../formatDate';

const Links = ({ links }: { links: Array<{ name: string; url: string }> | null }) => {
    if (!links || links.length === 0) return <></>;

    return (
        <>
            (
            {links.map((link, index) => (
                <React.Fragment key={link.name}>
                    <a href={link.url}>{link.name}</a>
                    {index !== links.length - 1 && ', '}
                </React.Fragment>
            ))}
            )
        </>
    );
};

export default function Experience() {
    return (
        <Section.Container>
            <Section.Header>Experience</Section.Header>
            {experience.map((job) => (
                <React.Fragment key={job.company}>
                    <Section.Row>
                        <Section.RowLeft>
                            <T.CompanyName>{job.company}</T.CompanyName>
                            <T.Side>
                                ({formatDate(job.start)}-{formatDate(job.end)})
                            </T.Side>
                        </Section.RowLeft>
                        <Section.Nested>
                            {job.projects.map((project, index) => (
                                <React.Fragment key={project.start}>
                                    <Section.Row>
                                        <Section.RowContent>
                                            {project.name && (
                                                <>
                                                    <T.ProjectName>{project.name}</T.ProjectName>{' '}
                                                    <Links links={project.links} /> as{' '}
                                                </>
                                            )}
                                            <T.Role>{project.role}</T.Role>
                                            {project.description && (
                                                <T.P>
                                                    <T.B>Description: </T.B>
                                                    <span dangerouslySetInnerHTML={{ __html: project.description}} />
                                                </T.P>
                                            )}
                                            {project.responsibilities && (
                                                <T.P>
                                                    <T.B>Responsibilities: </T.B>
                                                    {project.responsibilities}
                                                </T.P>
                                            )}
                                            {project.highlights && (
                                                <>
                                                    <T.P>
                                                        <T.B>Highlights: </T.B>
                                                    </T.P>
                                                    <ul>
                                                        {project.highlights.map((hl) => (
                                                            <li key={hl} dangerouslySetInnerHTML={{ __html: hl }} />
                                                        ))}
                                                    </ul>
                                                </>
                                            )}
                                            <Technologies list={project.technologies} />
                                        </Section.RowContent>
                                        <Section.RowRight>
                                            <T.Side>
                                                {formatDate(project.start)}
                                                <br />
                                                {formatDate(project.end)}
                                            </T.Side>
                                        </Section.RowRight>
                                    </Section.Row>
                                    {(index < job.projects.length - 1 || job.misc) && <hr />}
                                </React.Fragment>
                            ))}
                            {job.misc && (
                                <Section.Row>
                                    <Section.RowContent>
                                        <T.P>
                                            <T.B>Miscellaneous: </T.B>
                                        </T.P>
                                        <ul>
                                            {Object.entries(job.misc).map(([name, desc]) => (
                                                <li key={name}>
                                                    <strong>{name}</strong>:{' '}
                                                    <span dangerouslySetInnerHTML={{ __html: desc }} />
                                                </li>
                                            ))}
                                        </ul>
                                    </Section.RowContent>
                                </Section.Row>
                            )}
                        </Section.Nested>
                    </Section.Row>
                </React.Fragment>
            ))}
        </Section.Container>
    );
}
