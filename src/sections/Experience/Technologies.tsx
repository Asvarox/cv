import React from 'react';
import experience from '../../data/experience.json';
import T from '../components/Typgraphy';
import TechList from '../components/TechList';

interface Props {
    list: (typeof experience)[0]['projects'][0]['technologies'];
}

export default function Technologies({ list }: Props) {
    if (!list) return null;
    return (
        <>
            <T.P>
                <T.B>Technologies: </T.B>
                {Array.isArray(list) && <TechList list={list} />}
            </T.P>
            {!Array.isArray(list) && (
                <ul>
                    {Object.entries(list).map(([name, techs]) => (
                        <li key={name}>
                            <T.B>{name}</T.B>: <TechList list={techs} />
                        </li>
                    ))}
                </ul>
            )}
        </>
    );
}
