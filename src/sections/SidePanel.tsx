import styles from './SidePanel.module.css';
import classNames from 'classnames';

export default function SidePanel() {
    const toggleDarkTheme = () => {
        document.body.classList.toggle('dark-theme');
        localStorage.setItem('dark-theme', String(document.body.classList.contains('dark-theme')));
    };

    return (
        <div className={classNames(styles.sidePanel, 'hidden-print')}>
            <a
                href="#"
                className={classNames(styles.sidePanel__button, styles.sidePanel__button__light)}
                onClick={toggleDarkTheme}>
                Light Mode <i className="fas fa-sun"></i>
            </a>
            <a
                href="#"
                className={classNames(styles.sidePanel__button, styles.sidePanel__button__dark)}
                onClick={toggleDarkTheme}>
                Dark Mode <i className="fas fa-moon"></i>
            </a>
            <a href="./CV.pdf" className="side-panel__button">
                Download .pdf <i className="fas fa-file-pdf"></i>
            </a>
        </div>
    );
}
