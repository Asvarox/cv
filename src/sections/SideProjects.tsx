import React from 'react';
import sideProjects from '../data/side-projects.json';
import Section from './components/Section';
import T from './components/Typgraphy';
import Technologies from './Experience/Technologies';
import formatDate from '../formatDate';

export default function SideProjects() {
    return (
        <Section.Container>
            <Section.Header>Freelance/Side projects</Section.Header>
            {sideProjects.map((project) => (
                <React.Fragment key={project.name}>
                    <Section.Row>
                        <Section.RowLeft>
                            <T.CompanyName>
                                {project.name}
                                {project.url && (
                                    <>
                                        {' '}
                                        (<a href={project.url}>WWW</a>)
                                    </>
                                )}
                            </T.CompanyName>
                            <T.Side>
                                ({formatDate(project.start)}-{formatDate(project.end)})
                            </T.Side>
                        </Section.RowLeft>
                        <Section.Nested>
                            <Section.Row>
                                <Section.RowContent>
                                    {project.description && (
                                        <T.P>
                                            <T.B>Description: </T.B>
                                            {project.description}
                                        </T.P>
                                    )}
                                    <Technologies list={project.technologies} />
                                </Section.RowContent>
                            </Section.Row>
                        </Section.Nested>
                    </Section.Row>
                </React.Fragment>
            ))}
        </Section.Container>
    );
}
