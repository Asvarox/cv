import skills from '../data/skills.json';
import Section from './components/Section';
import T from './components/Typgraphy';
import TechList from './components/TechList';

export default function Skills() {
    return (
        <Section.Container>
            <Section.Header>Skills</Section.Header>
            {skills.map((skill) => (
                <Section.Row key={skill.name}>
                    <Section.RowLeft>
                        <T.CompanyName>{skill.name}</T.CompanyName>
                    </Section.RowLeft>
                    <Section.RowContent>
                        <T.P>
                            <TechList list={skill.sections} />
                        </T.P>
                    </Section.RowContent>
                </Section.Row>
            ))}
        </Section.Container>
    );
}
