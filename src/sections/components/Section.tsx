import styles from './Section.module.css';
import { PropsWithChildren } from 'react';
export function SectionContainer({ children }: PropsWithChildren) {
    return <section className={styles.sectionContainer}>{children}</section>;
}
export function SectionHeader({ children }: PropsWithChildren) {
    return <h2 className={styles.sectionHeader}>{children}</h2>;
}

export function SectionRow({ children }: PropsWithChildren) {
    return <div className={styles.sectionRow}>{children}</div>;
}
export function SectionNested({ children }: PropsWithChildren) {
    return <div className={styles.sectionNested}>{children}</div>;
}

export function SectionRowLeft({ children }: PropsWithChildren) {
    return <div className={styles.sectionRowLeft}>{children}</div>;
}
export function SectionRowContent({ children }: PropsWithChildren) {
    return <div className={styles.sectionRowContent}>{children}</div>;
}
export function SectionRowRight({ children }: PropsWithChildren) {
    return <div className={styles.sectionRowRight}>{children}</div>;
}

export default {
    Container: SectionContainer,
    Header: SectionHeader,
    Row: SectionRow,
    Nested: SectionNested,
    RowLeft: SectionRowLeft,
    RowContent: SectionRowContent,
    RowRight: SectionRowRight,
};
