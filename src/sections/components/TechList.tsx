import React from 'react';

interface Props {
    list: string[][];
}

export default function TechList({ list }: Props) {
    return <>{list.map((elem) => elem.join(', ')).join('; ')}</>;
}
