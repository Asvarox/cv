import styles from './Typography.module.css';
import { PropsWithChildren } from 'react';
function CompanyName({ children }: PropsWithChildren) {
    return <h3 className={styles.companyName}>{children}</h3>;
}
function ProjectName({ children }: PropsWithChildren) {
    return <span className={styles.companyName}>{children}</span>;
}
function Role({ children }: PropsWithChildren) {
    return <span className={styles.role}>{children}</span>;
}
function B({ children }: PropsWithChildren) {
    return <span className={styles.companyName}>{children}</span>;
}

function P({ children }: PropsWithChildren) {
    return <p className={styles.paragraph}>{children}</p>;
}
function Side({ children }: PropsWithChildren) {
    return <span className={styles.side}>{children}</span>;
}

export default {
    CompanyName,
    ProjectName,
    Role,
    P,
    B,
    Side,
};
