import { defineConfig } from 'vite';
import react from '@vitejs/plugin-react';
import tsconfigPaths from 'vite-tsconfig-paths';
import path from "node:path";
import { htmlPrerender } from 'vite-plugin-html-prerender';

export default defineConfig({
    plugins: [react(), tsconfigPaths(),
        htmlPrerender({
            staticDir: path.join(__dirname, 'dist'),
            routes: ['/'],
            selector: '#main',
            minify: {
                collapseBooleanAttributes: true,
                collapseWhitespace: true,
                decodeEntities: true,
                keepClosingSlash: true,
                sortAttributes: true,
            },
        }),],
    optimizeDeps: { include: ['react/jsx-dev-runtime'] },
    build: {
        minify: false,
    },
    base: './'
});
